<?php

use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use AppBundle\Command\CSVImportCommand;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Run test
 * vendor/phpunit/phpunit/phpunit -c app
 */
class CSVImportCommandTest extends KernelTestCase
{
    public function testExecute()
    {
        $kernel = $this->createKernel();
        $kernel->boot();

        $application = new Application($kernel);
        $application->add(new CSVImportCommand());

        $command = $application->find('import:csv');

        /**
         * Test command with nonexistent csv file
         */
        $failCommandTester = new CommandTester($command);
        $failCommandTester->execute([
            'command' => $command->getName(),
            'name' => 'fail'
        ]);
        $this->assertContains('File web/csv/fail.csv not found', $failCommandTester->getDisplay());

        /**
         * Test command with existed file
         */
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command' => $command->getName(),
            'name' => 'stock'
        ]);
        /**
         * Check if done and ids of skipped product
         */
        $this->assertContains('Done', $commandTester->getDisplay());
        $this->assertContains('P0007', $commandTester->getDisplay());
        $this->assertContains('P0011', $commandTester->getDisplay());
        $this->assertContains('P0015', $commandTester->getDisplay());
        $this->assertContains('P0017', $commandTester->getDisplay());
        $this->assertContains('P0027', $commandTester->getDisplay());
        $this->assertContains('P0028', $commandTester->getDisplay());
    }
}
