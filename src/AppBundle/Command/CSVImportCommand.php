<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Ddeboer\DataImport\Reader\CsvReader;

/**
 * For import use this command
 * php app/console import:csv stock
 * Run import in test mode
 * php app/console import:csv stock --test
 */
class CSVImportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('import:csv')
            ->setDescription('Import product')
            ->addArgument(
                'name',
                InputArgument::REQUIRED,
                'File for import'
            )
            ->addArgument(
                'batch-size',
                InputArgument::OPTIONAL,
                'File for import',
                100
            )
            ->addOption(
                'test',
                null,
                InputOption::VALUE_NONE,
                'Run script in test mode'
           )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $importService = $this->getContainer()->get('app.import');// Get service
        $name = $input->getArgument('name');
        $batchSize = $input->getArgument('batch-size');
        $testMode = $input->getOption('test');
        $filename = '';

        $filename = 'web/csv/' . $name . '.csv';

        if (!is_readable($filename) || !is_file($filename)) {
            $output->writeln('<error>File ' . $filename . ' not found</error>');
            return false;
        }

        $file = new \SplFileObject($filename); // Create and configure the reader

        $csvReader = new CsvReader($file);
        $csvReader->setHeaderRowNumber(0); // Tell the reader that the first row in the CSV file contains column headers
        $csvReader->setColumnHeaders($importService->getColumns()); // Set custom column headers

        if (!$csvReader->valid()) { //Check if file is valid
            $output->writeln('<error>Import file is not valid</error>');
            return false;
        }

        $importService->importProducts($csvReader, $testMode, $batchSize);

        $output->writeln('Total products: ' . $csvReader->count());
        $output->writeln('Successful added product: ' . $importService->getSuccessful());
        $output->writeln('Skipped product: ' . $importService->getSkipped());

        $failedProduct = $importService->getFailedProduct();

        if ($failedProduct) {
            foreach ($failedProduct as $key => $value) {
                $output->writeln($value);
            }
        }

        $output->writeln('<info>Done</info>');
    }
}
