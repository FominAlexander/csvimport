<?php

namespace AppBundle\Service;

use AppBundle\Entity\Product;

class ImportService
{
    protected $em; //Entity manager
    protected $validator; //Validator
    protected $skipped = 0; //Skipped products count
    protected $successful = 0; //Successful products count
    protected $failedProduct = []; //Array for failing imported products
    protected $columns = [ //CSV columns
        'productCode',
        'productName',
        'productDescription',
        'stock',
        'costGBP',
        'discontinued'
    ];

    /**
     * @param DoctrineORMEntityManager $em
     * @param SymfonyComponentValidatorValidatorRecursiveValidator $validator
     */
    public function __construct(\Doctrine\ORM\EntityManager $em,
                                \Symfony\Component\Validator\Validator\RecursiveValidator $validator)
    {
        $this->em = $em;
        $this->validator = $validator;
    }

    /**
     * Import products from CSV
     * @param  DdeboerDataImportReaderCsvReader $products
     * @param  boolean $testMode
     * @return boolean
     */
    public function importProducts(\Ddeboer\DataImport\Reader\CsvReader $products,
                                    $testMode = false,
                                    $batchSize)
    {
        $this->clearInfo();

        $errors = $products->getErrors(); //Get invalid rows

        if ($errors) {
            foreach ($errors as $key => $value) {
                $this->addToFailedProductArray($value);
            }
        }

        foreach ($products as $key => $product) {
            $this->importProduct($product);

            if(!$testMode && $key % $batchSize == 0) {
                $this->em->flush();
                $this->em->clear();
            }

        }

        if(!$testMode) {
            $this->em->flush();
            $this->em->clear();
        }

    }

    /**
     * Import single product
     * @param  Array  $productInfo
     * @return boolean
     */
    protected function importProduct(Array $productInfo)
    {
        if (!$this->checkStructure($productInfo)) {
            $this->addToFailedProductArray($productInfo);
            return false;
        }

        $product = new Product();
        $product->setCode($productInfo['productCode']);
        $product->setAdded(new \DateTime());
        $product->setName($productInfo['productName']);
        $product->setDescription($productInfo['productDescription']);
        $product->setStock($productInfo['stock']);
        $product->setCost($productInfo['costGBP']);
        $product->setStmptimestamp(new \DateTime());

        if (strtolower($productInfo['discontinued']) == 'yes') {
            $product->setDiscontinued(new \DateTime());
        }

        $errors = $this->validator->validate($product);

        if (count($errors) > 0) {
            $this->addToFailedProductArray($productInfo);
        } else {
            $this->em->persist($product);
            $this->successful++;
        }
    }

    /**
     * Add info about failed import to array
     * @param Array $product
     */
    public function addToFailedProductArray($product)
    {
        if ($product) {

            if (array_key_exists('productCode', $product) && array_key_exists('productName', $product)) {
                $this->failedProduct[$product['productCode']] = $product['productCode'] . ' - ' . $product['productName'];
            } else {
                $this->failedProduct[$product[0]] = $product[0] . ' - ' . $product[1];
            }

            $this->skipped++;
        }
    }

    /**
     * Check csv rows column
     * @param  Array $product
     * @return boolean
     */
    protected function checkStructure($product)
    {
        foreach ($this->columns as $key => $column) {
            if (!array_key_exists($column, $product)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Return count of successful imported product
     * @return integer successful
     */
    public function getSuccessful()
    {
        return $this->successful;
    }

    /**
     * Return count of skipped product
     * @return integer skipped
     */
    public function getSkipped()
    {
        return $this->skipped;
    }

    /**
     * Return array of skipped products
     * @return Array failedProduct
     */
    public function getFailedProduct()
    {
        sort($this->failedProduct);
        return $this->failedProduct;
    }

    /**
     * Return array of csv columns
     * @return Array columns
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     *  Clear object statistics
     */
    protected function clearInfo()
    {
        $this->skipped = 0;
        $this->successful = 0;
        $this->failedProduct = [];
    }
}
