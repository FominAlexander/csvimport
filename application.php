<?php
// application.php

require __DIR__.'/vendor/autoload.php';

use AppBundle\Command\CSVImportCommand;
use Symfony\Component\Console\Application;

$application = new Application();
$application->add(new CSVImportCommand());
$application->run();
